insert into todo (ID, USERNAME, DESCRIPTION, TARGET_DATE, DONE)
values(1, 'in28minutes', 'Get AWS Certified 2', CURRENT_DATE(), false);

insert into todo (ID, USERNAME, DESCRIPTION, TARGET_DATE, DONE)
values(2, 'in28minutes', 'Get Azure Certified 2', CURRENT_DATE(), false);

insert into todo (ID, USERNAME, DESCRIPTION, TARGET_DATE, DONE)
values(3, 'in28minutes', 'Get GCP Certified 2', CURRENT_DATE(), false);

insert into todo (ID, USERNAME, DESCRIPTION, TARGET_DATE, DONE)
values(4, 'in28minutes', 'Spring Mastery 2', CURRENT_DATE(), false);