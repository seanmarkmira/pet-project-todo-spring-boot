package com.in28minutes.springboot.myfirstwebapp.todo;

import java.time.LocalDate;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import jakarta.validation.Valid;

@Controller
@SessionAttributes("name") //This will be able to help us pass the values from different classes 
public class TodoControllerJPA {
	
	private TodoRepository todoRepository;
	
	public TodoControllerJPA(TodoRepository todoRepository) {
		super();
		this.todoRepository = todoRepository;
	}

	@RequestMapping("list-todos")
	public String listAllTodos(ModelMap model) {
		String username = getLoggedInUsername(model);
		List<Todo> todos = todoRepository.findByUsername(username);
		model.addAttribute("todos", todos);
		return "listTodos";
	}
	
	@RequestMapping(value="add-todo", method=RequestMethod.GET)
	public String showNewTodoPage(ModelMap model) {
		String username = getLoggedInUsername(model);
		//This is mapped to the todo.jsp
		//This approach is from controller to the form that is shown on the website
		//This is implementing 2 way binding - this is from the bean to the form -- see addNewTodo for the 2nd binding
		Todo todo = new Todo(0, username, "in show new todopage", LocalDate.now().plusYears(1), false);
		model.put("todo", todo);
		
		return "todo";
	}

	private String getLoggedInUsername(ModelMap model) {
		return (String)model.get("name");
	}
	
	@RequestMapping(value="add-todo", method=RequestMethod.POST)
	//the Todo todo in the parameter is part of the 2 way binding - here is that from the form to the bean
	//The incoming data is bound to the description - this is set in the JSP
	public String addNewTodo(ModelMap model, @Valid Todo todo, BindingResult result) {
		
		if(result.hasErrors()) {
			System.out.println(result);
			return "todo";
		}
		String username = getLoggedInUsername(model);
		System.out.println("Testing the data that is setting in the todo.jsp to our java file");
		System.out.println(todo.isDone());
		System.out.println("Testing the data that is setting in the todo.jsp to our java file");
		todo.setUsername(username);
		todoRepository.save(todo);
//		todoService.addTodo(username, todo.getDescription(), todo.getTargetDate(), todo.isDone());
		return "redirect:list-todos";
	}
	
	@RequestMapping("delete-todo")
	public String deletelTodos(@RequestParam int id) {
		todoRepository.deleteById(id);
		return "redirect:list-todos";
	}
	
	@RequestMapping(value = "update-todo", method = RequestMethod.GET)
	public String showUpdateTodoPage(@RequestParam int id, ModelMap model) {
		Todo todo = todoRepository.findById(id).get();
		model.addAttribute("todo", todo);
		return "todo";
	}
	
	@RequestMapping(value="update-todo", method=RequestMethod.POST)
	public String updateTodo(ModelMap model, @Valid Todo todo, BindingResult result) {
		
		if(result.hasErrors()) {
			System.out.println(result);
			return "todo";
		}
		
		String username = getLoggedInUsername(model);
		todo.setUsername(username);
		todoRepository.save(todo);
//		todoService.updateTodo(todo);
		return "redirect:list-todos";
	}
	
}
